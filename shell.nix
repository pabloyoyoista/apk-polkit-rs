let
  moz_overlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
  pkgs = import (fetchTarball("channel:nixpkgs-unstable")) { overlays = [ moz_overlay ]; };
  rustSrc =
    pkgs.latest.rustChannels.nightly.rust.override { extensions = [ "rust-src" ]; };
  buildInputs = with pkgs; [
    apk-tools
    cargo-outdated
    clang_13
    gdb
    glib
    findutils
    lld_13
    llvm_13.dev
    llvmPackages_13.libclang
    meson
    ninja
    openssl
    openssl.dev
    pkg-config
    python3
    rustfmt
    rustSrc
    zlib
  ];
in pkgs.mkShell {
  buildInputs = buildInputs;

  LIBCLANG_PATH = "${pkgs.llvmPackages_13.libclang.lib}/lib";
  RUST_SRC_PATH = "${rustSrc}/lib/rustlib/src/rust/src";
  RUSTFLAGS="-C linker=clang -C link-arg=--ld-path=${pkgs.lld_13}/bin/ld.lld";
  LD_LIBRARY_PATH = "${pkgs.lib.makeLibraryPath buildInputs}";
  RUST_BACKTRACE = 1;
}
