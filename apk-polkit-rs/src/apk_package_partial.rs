use crate::details_flags::DetailsFlags;
use apk_tools::{ApkPackage, ApkPackageError};
use enumflags2::BitFlags;
use serde::Serialize;
use std::{collections::HashMap, convert::TryFrom};
use zvariant::{OwnedValue, Str};

#[cfg(test)]
use zvariant::Value;

#[derive(Debug)]
pub struct ApkPackagePartial {
    inner: HashMap<&'static str, OwnedValue>,
}

impl zvariant::Type for ApkPackagePartial {
    fn signature() -> zvariant::Signature<'static> {
        zvariant::Signature::try_from("a{sv}").unwrap()
    }
}

impl ApkPackagePartial {
    pub fn empty() -> Self {
        ApkPackagePartial {
            inner: HashMap::new(),
        }
    }

    pub fn from_apk_package(
        package: ApkPackage<'_>,
        details_flags: BitFlags<DetailsFlags>,
    ) -> Self {
        let mut h = HashMap::new();
        h.insert("name", Str::from(package.name()).into());
        if details_flags.contains(DetailsFlags::Description) {
            if let Some(d) = package.description() {
                h.insert("description", Str::from(d).into());
            }
        }
        if details_flags.contains(DetailsFlags::InstalledSize) {
            h.insert("installed_size", (package.installed_size() as u64).into());
        }
        if details_flags.contains(DetailsFlags::Version) {
            h.insert("version", Str::from(package.version()).into());
        }
        if details_flags.contains(DetailsFlags::License) {
            if let Some(l) = package.license() {
                h.insert("license", Str::from(l).into());
            }
        }
        if details_flags.contains(DetailsFlags::PackageState) {
            h.insert("package_state", (package.package_state() as u32).into());
        }
        if details_flags.contains(DetailsFlags::Size) {
            h.insert("size", (package.size() as u64).into());
        }
        if details_flags.contains(DetailsFlags::StagingVersion) {
            if let Some(v) = package.staging_version() {
                h.insert("staging_version", Str::from(v).into());
            }
        }
        if details_flags.contains(DetailsFlags::Url) {
            if let Some(u) = package.url() {
                h.insert("url", Str::from(u).into());
            }
        }
        Self { inner: h }
    }

    pub fn from_apk_package_error(package: ApkPackageError) -> Self {
        let mut h = HashMap::new();
        h.insert("name", Str::from(package.name()).into());
        h.insert("error", Str::from(package.error_str()).into());
        Self { inner: h }
    }
}

impl PartialEq for ApkPackagePartial {
    fn eq(&self, other: &Self) -> bool {
        self.inner.get("name") == other.inner.get("name")
            && self.inner.get("description") == other.inner.get("description")
            && self.inner.get("version") == other.inner.get("version")
            && self.inner.get("license") == other.inner.get("license")
            && self.inner.get("package_state") == other.inner.get("package_state")
            && self.inner.get("staging_version") == other.inner.get("staging_version")
            && self.inner.get("url") == other.inner.get("url")
    }
}

impl Serialize for ApkPackagePartial {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.inner.serialize(serializer)
    }
}

pub trait ToPartial<T> {
    fn to_partial(self, details: BitFlags<DetailsFlags>) -> Vec<T>
    where
        Self: Sized;
}

impl ToPartial<ApkPackagePartial> for Vec<ApkPackage<'_>> {
    fn to_partial(mut self, details: BitFlags<DetailsFlags>) -> Vec<ApkPackagePartial>
    where
        Self: Sized,
    {
        self.drain(..)
            .map(|p| ApkPackagePartial::from_apk_package(p, details))
            .collect()
    }
}

impl ToPartial<ApkPackagePartial> for Vec<Result<ApkPackage<'_>, ApkPackageError>> {
    fn to_partial(mut self, details: BitFlags<DetailsFlags>) -> Vec<ApkPackagePartial>
    where
        Self: Sized,
    {
        self.drain(..)
            .map(|r| match r {
                Ok(p) => ApkPackagePartial::from_apk_package(p, details),
                Err(e) => ApkPackagePartial::from_apk_package_error(e),
            })
            .collect()
    }
}

#[cfg(test)]
impl ApkPackagePartial {
    pub fn builder() -> builder::ApkPackagePartialBuilder {
        builder::ApkPackagePartialBuilder::default()
    }

    pub fn is_err(&self) -> bool {
        self.inner.contains_key("error")
    }

    pub fn err(&self) -> String {
        let error: &str = Value::downcast_ref::<Str>(self.inner.get("error").unwrap())
            .unwrap()
            .as_str();
        String::from(error)
    }
}
#[cfg(test)]
mod builder {
    use super::ApkPackagePartial;
    use apk_tools::PackageState;
    use std::collections::HashMap;
    use zvariant::Str;

    #[derive(Debug, Clone, Default)]
    pub struct ApkPackagePartialBuilder {
        name: Option<String>,
        description: Option<String>,
        installed_size: Option<u64>,
        version: Option<String>,
        license: Option<String>,
        package_state: Option<PackageState>,
        size: Option<u64>,
        staging_version: Option<String>,
        url: Option<String>,
    }

    impl ApkPackagePartialBuilder {
        pub fn name(mut self, v: String) -> Self {
            self.name = Some(v);
            self
        }

        pub fn description(mut self, v: String) -> Self {
            self.description = Some(v);
            self
        }

        pub fn installed_size(mut self, v: usize) -> Self {
            self.installed_size = Some(v as u64);
            self
        }

        pub fn version(mut self, v: String) -> Self {
            self.version = Some(v);
            self
        }

        pub fn license(mut self, v: String) -> Self {
            self.license = Some(v);
            self
        }

        pub fn package_state(mut self, v: PackageState) -> Self {
            self.package_state = Some(v);
            self
        }

        pub fn size(mut self, v: usize) -> Self {
            self.size = Some(v as u64);
            self
        }

        pub fn staging_version(mut self, v: String) -> Self {
            self.staging_version = Some(v);
            self
        }

        pub fn url(mut self, v: String) -> Self {
            self.url = Some(v);
            self
        }

        pub fn build(self) -> ApkPackagePartial {
            let mut h = HashMap::new();
            if let Some(v) = self.name {
                h.insert("name", Str::from(v).into());
            }
            if let Some(v) = self.description {
                h.insert("description", Str::from(v).into());
            }
            if let Some(v) = self.installed_size {
                h.insert("installed_size", v.into());
            }
            if let Some(v) = self.version {
                h.insert("version", Str::from(v).into());
            }
            if let Some(v) = self.license {
                h.insert("license", Str::from(v).into());
            }
            if let Some(v) = self.package_state {
                h.insert("package_state", (v as u32).into());
            }
            if let Some(v) = self.size {
                h.insert("size", v.into());
            }
            if let Some(v) = self.staging_version {
                h.insert("staging_version", Str::from(v).into());
            }
            if let Some(v) = self.url {
                h.insert("url", Str::from(v).into());
            }
            ApkPackagePartial { inner: h }
        }
    }
}

#[cfg(test)]
mod test {
    use zvariant::Type;

    use super::ApkPackagePartial;

    #[test]
    fn signature() {
        assert_eq!(ApkPackagePartial::signature(), "a{sv}");
    }
}
