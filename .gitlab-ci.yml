variables:
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - qa
  - build
  - sanitize
  - coverage

.default:
  image: cogitri/apk-polkit-rs-ci:debian-nightly-1672
  tags:
    - ci-build
    - x86_64

.build-debian:
  extends: .default
  script:
    - rm -rf build
    - meson build && ninja -C build
    - dbus-run-session -- env LD_LIBRARY_PATH=/usr/local/lib/x86_64-linux-gnu meson test -v -C build
  artifacts:
    when: on_failure
    paths:
      - build/meson-logs/testlog.txt

fmt:
  extends: .default
  stage: qa
  script:
    - rustup default stable
    - rustup component add rustfmt
    - cargo fmt -- --check

clippy:
  extends: .default
  stage: qa
  script:
    - rustup default stable
    - rustup component add clippy
    - cargo clippy --all

build-debug-debian:
  extends: .build-debian
  stage: build

build-debug-debian-sanitizer:
  extends: .build-debian
  stage: sanitize
  before_script:
    - sed "s|cargo_options,|[cargo_options, '--target', 'x86_64-unknown-linux-gnu'],|" -i meson.build # https://github.com/rust-analyzer/rust-analyzer/issues/2904#issuecomment-578508740
    - sed "s|/ rust_target /|/ 'x86_64-unknown-linux-gnu' / rust_target /|" -i meson.build
    - sed "s|'--all'|'--target', 'x86_64-unknown-linux-gnu', '--all'|" -i meson.build
  variables:
    RUSTFLAGS: "-Z sanitizer=address"
    RUST_TARGET: "x86_64-unknown-linux-gnu"

build-debug-debian-valgrind-x86:
  extends: .build-debian
  stage: sanitize
  before_script:
    - DEBIAN_FRONTEND=noninteractive apt-get install -y valgrind
  script:
    - meson build && ninja -C build && dbus-run-session -- meson test --wrap=valgrind -v -C build

build-debug-debian-coverage:
  extends: .build-debian
  stage: coverage
  before_script:
    - rm -rf build
  after_script:
    - grcov ./build/debug/ --ignore "*-sys*" -s . -t html --llvm --branch --ignore-not-existing -o ./build/debug/coverage/
    - grcov ./build/debug/ --ignore "*apk-tools-sys*" --ignore "*bitflags*" --ignore "*derivative*" --ignore "*enumflags*" --ignore "*getrandom" --ignore "*zvariant*"  --ignore "*zbus*" --ignore "*void*" --ignore "*version*"  --ignore "*unicode*" --ignore "*toml*" --ignore "*thiserror*" --ignore "*tempfile*"  --ignore "*system-deps*" --ignore "*syn*" --ignore "*strum*" --ignore "*serde*"  --ignore "*rand*" --ignore "*proc*" --ignore "*nix*" --ignore "*heck*" --ignore "*byteorder*" --ignore "*libc*" --ignore "*log*" --ignore "*pkg-config*" --ignore "*ppv*" --ignore "*quote*" --ignore "*scoped-tls*" --ignore "*addr2line*" --ignore "*adler*" --ignore "*aho-coresick*" --ignore "*backtrace*" --ignore "*cc*" --ignore "*cc*" --ignore "*error-chain*" --ignore "*fern*" --ignore "*gimli*" --ignore "*lazy_static*" --ignore "*locale_config*" --ignore "*memchr*" --ignore "*miniz_oxide*" --ignore "*object*" --ignore "*regex"* --ignore "*rustc-demangle*" --ignore "*thread_local*" --ignore "*time*" -s . --llvm --branch --ignore-not-existing -o ./build/debug/coverage/lcov.info
    - lcov -l ./build/debug/coverage/lcov.info
  coverage: '/Total:\| *(\d+\.?\d+\%)/'
  artifacts:
    paths:
      - build/debug/coverage
  variables:
    CARGO_INCREMENTAL: 0
    RUSTFLAGS: "-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off -Zpanic_abort_tests"
    RUSTDOCFLAGS: "-Cpanic=abort"
