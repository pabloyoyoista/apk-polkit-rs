/*
    Copyright: 2020 Rasmus Thomsen <oss@cogitri.dev>
    SPDX-License-Identifier: GPL-3.0-or-later
*/

#![deny(clippy::all)]
#![allow(clippy::new_without_default)]
#![warn(clippy::await_holding_refcell_ref)]
#![warn(clippy::cast_lossless)]
#![warn(clippy::comparison_to_empty)]
#![warn(clippy::manual_find_map)]
#![warn(clippy::map_unwrap_or)]
#![warn(clippy::struct_excessive_bools)]
#![warn(clippy::unnecessary_unwrap)]
#![warn(clippy::trivially_copy_pass_by_ref)]
#![allow(clippy::must_use_candidate)]

pub mod apk_arrays;
pub mod apk_database;
pub mod apk_package;
pub mod apk_repository;
pub mod error;
pub mod i18n;

pub use crate::apk_database::*;
pub use crate::apk_package::*;
pub use crate::apk_repository::*;
pub use crate::error::*;
