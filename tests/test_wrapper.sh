#!/bin/bash
#
# Copyright (C) 2022 Pablo Correa Gomez
#

set -ex

python3 -m dbusmock --session --template polkitd &
sleep 1

trap 'kill %1' EXIT

gdbus call --session --dest org.freedesktop.PolicyKit1 \
           --object-path /org/freedesktop/PolicyKit1/Authority \
           --method org.freedesktop.DBus.Mock.AllowUnknown true

"$@"
